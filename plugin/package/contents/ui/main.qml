/*
 *  SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.20 as Kirigami
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm
import org.kde.kaccounts.matrix 1.0

Kirigami.ApplicationWindow {
    id: matrixWindow

    minimumWidth: Kirigami.Units.gridUnit * 20
    minimumHeight: Kirigami.Units.gridUnit * 35
    width: Kirigami.Units.gridUnit * 24
    height: minimumHeight
    onClosing: {
        MatrixController.cancel();
    }

    Timer {
        id: timer

        onTriggered: {
            if (userId.text.length > 0)
                MatrixController.tryConnecting(userId.text);

        }
    }

    pageStack.initialPage: Kirigami.Page {
        title: i18nc("@title", "Matrix")

        ColumnLayout {
            width: parent.width

            MobileForm.FormCard {
                Layout.topMargin: Kirigami.Units.largeSpacing
                Layout.fillWidth: true

                contentItem: ColumnLayout {
                    spacing: 0

                    MobileForm.FormCardHeader {
                        title: i18n("Login to Matrix")
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: userId

                        label: i18n("User ID:")
                        placeholderText: i18n("@user:kde.org")
                        onAccepted: continueButton.clicked()
                        inputMethodHints: Qt.ImhUrlCharactersOnly | Qt.ImhNoPredictiveText
                        onTextChanged: {
                            timer.restart();
                            MatrixController.reachable = false
                        }
                    }

                    MobileForm.FormTextFieldDelegate {
                        id: password
                        visible: false
                        label: i18n("Password:")
                        placeholderText: i18n("")
                        onAccepted: continueButton.clicked()
                        inputMethodHints: Qt.ImhUrlCharactersOnly | Qt.ImhNoPredictiveText
                        echoMode: TextInput.Password
                    }

                    MobileForm.FormDelegateSeparator {
                        above: continueButton
                    }

                    MobileForm.FormButtonDelegate {
                        id: continueButton

                        text: i18n("Continue")
                        enabled: MatrixController.reachable
                        onClicked: {
                            if (userId.visible) {
                                userId.visible = false;
                                password.visible = true;
                            } else {
                                MatrixController.tryLogin(password.text)
                                continueButton.enabled = false
                            }
                        }
                    }

                }

            }

        }

    }

}
