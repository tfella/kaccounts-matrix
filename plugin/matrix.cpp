// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "matrix.h"

#include <KDeclarative/QmlObject>
#include <KLocalizedString>
#include <KPackage/PackageLoader>

#include <QIcon>
#include <QQmlContext>
#include <QQmlEngine>
#include <QWindow>

#include "matrixcontroller.h"

MatrixWizard::MatrixWizard(QObject *parent)
    : KAccountsUiPlugin(parent)
{
    qmlRegisterSingletonInstance("org.kde.kaccounts.matrix", 1, 0, "MatrixController", &MatrixController::instance());
}

void MatrixWizard::init(KAccountsUiPlugin::UiType type)
{
    if (type == KAccountsUiPlugin::NewAccountDialog) {
        const QString packagePath(QStringLiteral("org.kde.kaccounts.matrix"));
        m_object = new KDeclarative::QmlObject();
        m_object->setTranslationDomain(packagePath);
        m_object->setInitializationDelayed(true);

        KPackage::Package package = KPackage::PackageLoader::self()->loadPackage(QStringLiteral("KPackage/GenericQML"));
        package.setPath(packagePath);
        m_object->setSource(QUrl::fromLocalFile(package.filePath("mainscript")));
        m_data = package.metadata();

        connect(&MatrixController::instance(), &MatrixController::wizardFinished, this, [this](const QString &username, const QString &password) {
            m_object->deleteLater();
            Q_EMIT success(username, password, {});
        });

        connect(&MatrixController::instance(), &MatrixController::wizardCancelled, this, [this] {
            m_object->deleteLater();
            Q_EMIT canceled();
        });

        m_object->completeInitialization();

        if (!m_data.isValid()) {
            return;
        }

        Q_EMIT uiReady();
    }
}

void MatrixWizard::setProviderName(const QString &providerName)
{
    Q_UNUSED(providerName)
}

void MatrixWizard::showNewAccountDialog()
{
    QWindow *window = qobject_cast<QWindow *>(m_object->rootObject());
    if (window) {
        window->setTransientParent(transientParent());
        window->show();
        window->requestActivate();
        window->setTitle(m_data.name());
        window->setIcon(QIcon::fromTheme(m_data.iconName()));
    }
}

void MatrixWizard::showConfigureAccountDialog(const quint32 accountId)
{
    Q_UNUSED(accountId)
}

QStringList MatrixWizard::supportedServicesForConfig() const
{
    return QStringList();
}
