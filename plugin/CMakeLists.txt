add_library(matrix_plugin_kaccounts MODULE
    matrix.cpp
    matrixcontroller.cpp
)

target_link_libraries(matrix_plugin_kaccounts
    Qt5::Core
    KF5::I18n
    KF5::Declarative
    KF5::Package
    KAccounts
    Quotient
)

install(TARGETS matrix_plugin_kaccounts
    DESTINATION ${PLUGIN_INSTALL_DIR}/kaccounts/ui
)

kpackage_install_package(package org.kde.kaccounts.matrix genericqml)
