// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <KPluginMetaData>
#include <QHash>
#include <QUrl>

#include <kaccountsuiplugin.h>

namespace KDeclarative
{
class QmlObject;
}

class MatrixWizard : public KAccountsUiPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kaccounts.UiPlugin")
    Q_INTERFACES(KAccountsUiPlugin)

public:
    explicit MatrixWizard(QObject *parent = nullptr);

    virtual void init(KAccountsUiPlugin::UiType type) override;
    virtual void setProviderName(const QString &providerName) override;
    virtual void showNewAccountDialog() override;
    virtual void showConfigureAccountDialog(const quint32 accountId) override;
    virtual QStringList supportedServicesForConfig() const override;

private:
    QHash<QString, int> m_services;
    KDeclarative::QmlObject *m_object;
    KPluginMetaData m_data;
};
