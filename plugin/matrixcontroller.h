// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>
#include <QPointer>
#include <QString>

namespace Quotient
{
class Connection;
}

class MatrixController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool reachable READ reachable WRITE setReachable NOTIFY reachableChanged)
public:
    static MatrixController &instance()
    {
        static MatrixController _instance;
        return _instance;
    }

    [[nodiscard]] bool reachable() const;
    Q_INVOKABLE void tryConnecting(const QString &userId);
    Q_INVOKABLE void tryLogin(const QString &password);
    void setReachable(bool reachable);

Q_SIGNALS:
    void wizardFinished(const QString &userName, const QString &password);
    void wizardCancelled();
    void reachableChanged();

private:
    MatrixController();
    bool m_reachable = false;
    QPointer<Quotient::Connection> m_connection;
    QString m_userId;
};
