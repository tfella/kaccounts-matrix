// SPDX-FileCopyrightText: 2023 Tobias Fella <tobias.fella@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "matrixcontroller.h"
#include <connection.h>
#include <qt_connection_util.h>

using namespace Quotient;

MatrixController::MatrixController()
    : QObject(nullptr)
    , m_connection(new Connection(this))
{
}

bool MatrixController::reachable() const
{
    return m_reachable;
}

void MatrixController::setReachable(bool reachable)
{
    m_reachable = reachable;
    Q_EMIT reachableChanged();
}

void MatrixController::tryConnecting(const QString &userId)
{
    setReachable(false);
    m_userId = userId;
    // TODO support SSO
    m_connection->resolveServer(userId);
    connectSingleShot(m_connection.data(), &Connection::loginFlowsChanged, this, [this] {
        setReachable(m_connection->loginFlows().contains(LoginFlows::Password));
    });
}

void MatrixController::tryLogin(const QString &password)
{
    m_connection->loginWithPassword(m_userId, password, QStringLiteral("KAccounts"), {});
    connect(m_connection, &Connection::connected, this, [this, password] {
        Q_EMIT wizardFinished(m_userId, password);
        m_connection->logout();
        connect(m_connection, &Connection::loggedOut, m_connection, &Connection::deleteLater);
    });
    connect(m_connection, &Connection::loginError, this, [this](QString error, const QString &) {
        // TODO
    });
}